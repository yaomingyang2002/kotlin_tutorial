/*
Java doesn't supports default function, when call for interoperability from kotlin file use @JvmOverloads

MyJavaFile.java:
    public class MyJavaFile {

        public static void main(String[] args) {

            int result = MyKotlinInteroperabilityKt.findTheVolume(3, 4, 40);
            System.out.println(result);
        }

        public static int getArea(int l, int b) {
            return l * b;
        }
    }


MyKotlinInteroperability.kt:

fun main(args: Array<String>) {

    var result = findTheVolume(breadth = 2, length = 3)
    println(result)

    var result1 = findTheVolume(2, 3, 30) //Overrides the defalt value
    print(result1)
}


@JvmOverloads
fun findTheVolume(length: Int, breadth: Int, height: Int = 10): Int {//with default params height

    return length * breadth * height
}

* */