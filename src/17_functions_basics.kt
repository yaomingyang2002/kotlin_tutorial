
/*
*   FUNCTIONS Basics
*
*   fun findArea(length: Int, breadth: Int) : Int{
*       return length * breadth
*    }
*
*    fun findArea(length: Int, breadth: Int) : Unit{ //void in Java, don't return anything
*       print (length * breadth)
*    }
*     or
*
*    fun findArea(length: Int, breadth: Int) { //void in Java, don't return anything
*       print (length * breadth)
*    }
* */

fun main(args: Array<String>) {


    var sum = add(2, 4)

    println("Sum is " + sum)
}

fun add(a: Int, b: Int): Int {
    return a + b
}
