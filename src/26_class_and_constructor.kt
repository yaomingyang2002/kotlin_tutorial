
/*
*   Class, Primary Constructor, Secondary Constructor and Init Block
*   primary constructors:
*   - init block
*   - primary constructor with property
*   -primary constructor with just parameters
*
*   Secondary constructors
* */
fun main(args: Array<String>) {

    var student = Student("Steve", 10) //not new KW as in  java, call all constructors

    println(student.id)
}

class Student(var name: String) { //(primary constructor with property, var), can omit the constructor keyword

    var id: Int = -1 //property

    init {//class initializer block
        println("Student has got a name as $name and id is $id")
    }

    constructor(n: String, id: Int): this(n) {//Secondary constructor, n=name need to be called, var not allowed
        // The body of the secondary constructor is called after init block
        this.id = id
    }
}
