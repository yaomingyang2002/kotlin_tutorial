
/*
*   Data Class
*
*   super class "Any" contains functions:
*   -equals(): Boolean
*   -hashCode(): Int
*   -toString():String
*   -copy() //Kotlin
*
*   Data classes provides these mentioned methods
* */
fun main(args: Array<String>) {

    var user1 = User("Sam", 10) //one object

    var user2 = User("Sam", 10) //another object

    println(user1.toString())

    if (user1 == user2)
        println("Equal")
    else
        println("Not equal")

    var newUser = user1.copy(id = 25) //copy and override the id =25
    println(newUser)
}

data class User(var name: String, var id: Int)
