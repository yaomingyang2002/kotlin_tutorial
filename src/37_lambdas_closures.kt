
/*
*   1. Closures
*
*   in java 8, you cannot mutate values of outside variable inside Lambdas,
*   but in Kotlin you can change the values
* */
fun main(args: Array<String>) {

    val program = TheProgram()

    var result = 0 //outside variable value

    program.addTwoNumbers(2, 7) {x, y -> result = x + y} //lambda change the var result value

    println(result)
}

class TheProgram {

    fun addTwoNumbers(a: Int, b: Int, action: (Int, Int) -> Unit) {  // High Level Function with Lambda as Parameter

        action(a, b)
    }
}
