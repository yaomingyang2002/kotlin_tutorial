
/*
*   1. Map and HashMap
*
*   Collections:
    -> Immutable Collections: read only operations
        . immutable List: listOf
        .immutable Map: mapOF
        .immutable Set setOf

    -> Mutable collections: Read and Write both
        . Mutable List: ArrayList, arrayListOf, mutableListOf //all are same
        . Mutable Map: HashMap, hashMapOf, mutableMapOf
        .Mutable Set: mutableSetOf, hashSetOf
* */
fun main(args: Array<String>) {

    // Map Tutorial: Key-Value pair
    var myImutableMap = mapOf<Int, String>(4 to "Yogi", 43 to "Manmohan", 7 to "Modi" ) //immutable, fixed size, unordered

//    var myMap = HashMap<Int, String>()      // Mutable, READ and WRITE both, No Fixed Size
//    var myMap = mutableMapOf<Int, String>() // Mutable, READ and WRITE both, No Fixed Size
    var myMap = hashMapOf<Int, String>()      // Mutable, READ and WRITE both, No Fixed Size

    myMap.put(4, "Yogi") //  NOTE  to .put() not .add()
    myMap.put(43, "Manmohan")
    myMap.put(7, "Vajpayee")

    myMap.put(43, "Modi") // can use .replace(posi, value)

    for (key in myImutableMap.keys) {
        println("Element at $key = ${myImutableMap[key]}")  // myImutableMap.get(key)
    }

    println()

    for (key in myMap.keys) {
        println("Element at $key = ${myMap[key]}")  // myMap.get(key)
    }
}
