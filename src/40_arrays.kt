
/*

Array: Mutable but has fixed size, in python don't have fixed size!

Collections:
    -> Immutable Collections: read only operations
        . immutable List: listOf
        .immutable Map: mapOF
        .immutable Set setOf

    -> Mutable collections: Read and Write both
        . Mutable List: ArrayList, arrayListOf, mutableListOf
        . Mutable Map: HashMap, hashMapOf, mutableMapOf
        .Mutable Set: mutableSetOf, hashSetOf

*   1. Arrays
* */
fun main(args: Array<String>) {

    // Elements :   32  0   0   54  0
    // Index    :   0   1   2   3   4
    //Array<DataType>(size){init value} //declare array

    var myArray = Array<Int>(5) { 0 }   // Mutable. Fixed Size.
    myArray[0] = 32 // modify/change the element
    myArray[3] = 54
    myArray[1] = 11

    for (element in myArray) {              // Using individual elements (Objects)
        println(element)
    }

    println()

    for (index in 0..myArray.size - 1) {
        println(myArray[index])
    }
}
