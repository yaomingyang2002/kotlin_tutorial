
/*
*   Default Functions
* */
fun main(args: Array<String>) {

    var result = findVolume(2, 3)
    print(result)
}

fun findVolume(length: Int, breadth: Int, height: Int = 10): Int {

    return length * breadth * height
}

/*
To open REPL to check output->
Tools/Kotlin/Kotlin REPL (like pyhton shell)
CMD+Enter to execute
:quit for quit
 */