
/*
*   INFIX FUNCTIONS
*
*   Infix funcitons can be a Member function or extension function of a class
*   they have single parameter
*   they have prefix of infix
* */
fun main(args: Array<String>) {

    val x: Int = 15
    val y: Int = 10

    val greaterVal = x findGreaterValue y   //only can be used for infix func. = x.findGreaterValue(y)

    println(greaterVal)
}

infix fun Int.findGreaterValue(other: Int): Int {       // INFIX and Extension Func

    if (this > other)
        return this
//    else
//        return other
    return other
}

/*
*       1. All INFIX Functions are Extension functions
*            But all Extension functions are not INFIX?
*       2. INFIX Functions just have ONE PARAMETER
* */
