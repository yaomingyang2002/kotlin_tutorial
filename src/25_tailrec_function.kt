import java.math.BigInteger

/*
*   Tailrec Function : Recursive Functions, calling itwons function inside the function
*       -> Prevents Stackoverflow Exception
*
*   use recursion in Optimised way! similar to python generator that reduce to use memory
*   prefix of "tailrec" is used
*
*   Fibonacci Series
*       0  1  1  2  3  5  8  13  21 ......
* */
fun main(args: Array<String>) {

    println(getFibonacciNumber(6, BigInteger("0"), BigInteger("1")))
}

tailrec fun getFibonacciNumber(n: Int, a: BigInteger, b: BigInteger): BigInteger {// n is the index or position number

    if (n == 0)
        return  b
    else
        return getFibonacciNumber(n - 1, a + b, a)
}
