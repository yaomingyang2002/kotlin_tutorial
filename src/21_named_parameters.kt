

/*
*   Named Parameters
*
*   a pure Kotlin feature not present in java!
* */

fun main(args: Array<String>) {

    var result = findTheVolume(breadth = 2, length = 3) //note the order change of the named params
    println(result)

    var result1 = findTheVolume(2, 3, 30) //Overrides the defalt value
    print(result1)
}

fun findTheVolume(length: Int, breadth: Int, height: Int = 10): Int {//with default params height

    return length * breadth * height
}

