/*

1. Null Safety operators
2. 'lateinit' keyword
3. 'Lazy' initialization

Null Safety:
?. safe call operator
?: Elvis-operator
!! Not-null Assertion
?.let{} safe call with let

 */

fun main(args: Array<String>) {

    val country = Country()

//    country.name = "India"
//    println(country.name)

    country.setup()
}

class Country {

    lateinit var name: String

    fun setup() {
        name = "USA"
        println("The name of country is $name")
    }
}

// lateinit used only with mutable data type [ var ], not [val]
// lateinit used only with non-nullable data type, var name: String, but not var name: String?
// lateinit values must be initialised before you use it

// If you try to access lateinit variable without initializing it then it throws UninitializedPropertyAccessException
