
/*
*   Extension Functions: EXAMPLE ONE
*
*   adds new function to the classes
*   -can add functions to a class without declaring it
*   -the new functions added behaves like static
*
*   few properties:
*   -they can become part of your own class
*   -they can become part of predefined classes, e.g. String, Int, Array, etc
*
*   Benefits:
*   -reduces code
*   -code is much cleaner and easy to read
*
* */
fun main(args: Array<String>) {

    var student = Studentt()    // an instance of the Studentt class
    println("Pass status: " + student.hasPassed(57))

    println("Scholarship Status: " + student.isScholar(57)) //here call the function in the instance
}

fun Studentt.isScholar(marks: Int): Boolean { //here add/extend a function to the class Studentt
    return marks > 95
}

class Studentt {         // OUR OWN CLASS

    fun hasPassed(marks: Int): Boolean {
        return marks > 40
    }
}