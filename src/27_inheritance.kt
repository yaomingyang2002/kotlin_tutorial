
/*
*   Inheritance: object acquires all the properties from its' parent class object
*   1. single inheritance: Any-> Class A -> Class B
*   2. muillevel inheritance: Any-> Class A -> Class B -> Class C
*   3. Hierachial inheritance: Any-> Class A -> Class B,  Any-> Class A -> Class C
*
*   Any class contains functions such as:
*   -> equals(): Boolean
*   ->hashCode(): Int
*   -> toString(): String
*
*   Advantages:
*   . code resuability
*   . methods overriding
* */
fun main(args: Array<String>) {

    var dog = Dog()
    dog.bread = "labra"
    dog.color = "black"
    dog.bark()
    dog.eat()

    var cat = Cat()
    cat.age = 7
    cat.color = "brown"
    cat.meow()
    cat.eat()

    var animal = Animal()
    animal.color = "white"
    animal.eat()
}

open class Animal {         // Super class / Parent class /  Base class, ||open kw need for inheritant

    var color: String = ""

    fun eat() {
        println("Eat")
    }
}

class Dog : Animal() {      // Sub class / Child class / Derived class | : indicate inheritate

    var bread: String = ""

    fun bark() {        //| extension
        println("Bark")
    }
}

class Cat : Animal() {      // Sub class / Child class / Derived class

    var age: Int = -1

    fun meow() {  //| extension
        println("Meow")
    }
}
