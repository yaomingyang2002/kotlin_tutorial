
/*
*   1. List and ArrayList
*
*   Collections:
    -> Immutable Collections: read only operations
        . immutable List: listOf
        .immutable Map: mapOF
        .immutable Set setOf

    -> Mutable collections: Read and Write both
        . Mutable List: ArrayList, arrayListOf, mutableListOf //all are same
        . Mutable Map: HashMap, hashMapOf, mutableMapOf
        .Mutable Set: mutableSetOf, hashSetOf
* */
fun main(args: Array<String>) {

    // Elements :
    // Index    :   0   1   2   3   4

//             Array<DataType>(size){init value}       //declare array
//    var myArray = Array<Int>(5) { 0 }             // Mutable. Fixed Size.

    var myList = listOf<String>("Yogi", "Mike", "David") //Immutable, fixed size, read only

//    var list = mutableListOf<String>()  // Mutable, No Fixed Size, Can Add or Remove Elements
//    var list = arrayListOf<String>()    // Mutable, No Fixed Size, Can Add or Remove Elements
    var list = ArrayList<String>()      // Mutable, No Fixed Size, Can Add or Remove Elements
    list.add("Yogi")        // 0
    list.add("Manmohan")    // 1
    list.add("Vajpayee")    // 2

//    list.remove("Manmohan")
//    list.add("Vajpayee")

    list[1] = "Modi"

    for (element in myList) {             // Using individual elements (Objects)
        println(element)
    }

    println()

    for (index in 0..list.size - 1) { // using Index of the element (Objects)
        println(list[index])        //list.get(index)
    }
}
