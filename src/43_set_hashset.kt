
/*
*   1. Set and HashSet
*
*   Collections:
    -> Immutable Collections: read only operations
        . immutable List: listOf
        .immutable Map: mapOF
        .immutable Set setOf

    -> Mutable collections: Read and Write both
        . Mutable List: ArrayList, arrayListOf, mutableListOf //all are same
        . Mutable Map: HashMap, hashMapOf, mutableMapOf
        .Mutable Set: mutableSetOf, hashSetOf
* */
fun main(args: Array<String>) {

    // "Set" contains unique elements
    // "HashSet" also contains unique elements but sequence is not guaranteed in output
    var myImmutableSet = setOf<Int>( 2, 54, 3, 1, 0, 9, 9, 9, 8) //immutable, Read only

    var mySet = mutableSetOf<Int>( 2, 54, 3, 1, 0, 9, 9, 9, 8)  // Mutable Set, READ and WRITE both
//    var mySet = hashSetOf<Int>( 2, 54, 3, 1, 0, 9, 9, 9, 8)     // Mutable Set, READ and WRITE both

    mySet.remove(54)
    mySet.add(100)

    for (element in myImmutableSet) {
        println(element)        //output only unique element! ordered!
    }
    println()

    for (element in mySet) {
        println(element)        //output only unique element! unordered
    }
}
