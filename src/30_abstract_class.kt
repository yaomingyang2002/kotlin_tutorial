
/*
*   Abstract Class
*
*   visibility modifiers
*   Kotlin Suppoerts:
*   - public , by default
*   -Protected: inside subclass
*   -internal: same module
*   -private: see within class
* */
fun main(args: Array<String>) {

//    var person = MyPerson()   // Not allowed. You cannot create instance of abstract class

    var person = Indian()       // Allowed. Abstract Super class reference variable
                                // pointing to child class object.
    person.name = "Steve"
    person.eat()
    person.goToSchool()
}

abstract class MyPerson {     // you cannot create instance of abstract class

    abstract var name: String //abstract properties/methods just defined and no contents

    abstract fun eat()      // abstract properties are 'open' by default, method not body

    open fun getHeight() {} // A 'open' function ready to be overridden

    fun goToSchool() {}     // A normal function, cannot be overrided
}

class Indian: MyPerson() {// need to override the abstract properties and methods

    override var name: String = "dummy_indian_name"

    override fun eat() {
        // Our own code
    }
}