/*
Interoperability:
you can call java functions from Kotlin and Kotlin functions from java
- you can have JAVA and KOTLIN files within the SAME project,
- if they localize in different folder, you can import file from the folder

for Interoperability example in same folder see:

MyJavaFile.java:
    public class MyJavaFile {

        public static void main(String[] args) {

            int sum = MyKotlinInteroperabilityKt.addNumbers(3, 4);
            System.out.println("Printing sum from Java file :" + sum);
        }

        public static int getArea(int l, int b) {
            return l * b;
        }
    }


myKotlinInteroprability.kt:

    fun main(args: Array<String>) {

        var area = MyJavaFile.getArea(10, 5)
        println("Printing area from Kotlin file: $area")
    }

    fun addNumbers(a: Int, b: Int): Int {
        return a + b
    }


 */