
/*
* Explore Data Types in Kotlin
* Boolean,
* Byte,
* Char, 'a', '2'
* Short,
* Int,
* Long,
* Float, 3.4f, 3.7F
* Double, 3.4d, 3.7D
*
* var vs val
* */

fun main(args: Array<String>) {
//all data type in Kotlin are Objects! -> data type begin with Capitalized e.g. String, Int, Boolean, Double, Char
    // in java all begin with small letter!

    var name: String
    name = "Kevin"

    var age: Int = 10
    var myAge = 10

    var isAlive: Boolean = true
    var marks: Float = 97.4F
    var percentage: Double = 90.78
    var gender: Char = 'M'

    print(marks)
}

/*
To open REPL to check output->
Tools/Kotlin/Kotlin REPL (like pyhton shell)
CMD+Enter to execute
:quit for quit
 */