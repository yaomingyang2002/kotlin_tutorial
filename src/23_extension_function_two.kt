
/*
*   Extension Functions: EXAMPLE TWO
*
*   adds new function to the classes
*   -can add functions to a class without declaring it
*   -the new functions added behaves like static
*
*   few properties:
*   -they can become part of your own class
*   -they can become part of predefined classes, e.g. String, Int, Array, etc
*
*   Benefits:
*   -reduces code
*   -code is much cleaner and easy to read
* */

fun main(args: Array<String>) {

    var str1: String = "Hello " //String is Object (an instance of String class)
    var str2: String = "World"

    var str3: String = "Hey "

    println(str3.add(str1, str2)) //call the extended String.add function

    val x: Int = 6  //Int is Object (an instance of Int class)
    val y: Int = 10

    val greaterVal = x.greaterValue(y)  //call the extended Int.greaterValue function

    println(greaterVal)
}

fun String.add(s1: String, s2: String): String { // extend a add function to the predefined String class

    return this + s1 + s2
}

fun Int.greaterValue(other: Int): Int {// extend a greaterValue function to the predefined Int class

    if (this > other)
        return this
    else
        return other
}
