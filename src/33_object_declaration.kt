
/*
*   1. Object Declaration
*
*   Singleton: when we have just one instance of a class in the whole appplication.
*   - meaning: if Class Student is declared Singleton then we cannot create Ojbects for this class.
*       - There exists only one object for this class by default
*       -and we cannot create objects like student1, student2 etc
*
*   In Jave, we define Singleton by usig keyword "static" variables and methods
*   in Kotlin we declare "object", this creates a Singleton object when program runs
*
*   when we use keyword"object":
*   -Kotlin internally, creates a class and an object/instance.
*
*   These objects:
*   -can have properties, methods and initializers
*   -cannot have Constructors //cannot instantiate!
*   -can also have supper class and support Inheritance
*
* */
fun main(args: Array<String>) {

    CustomersData.count = 98 //don't instantiate anymore
    CustomersData.typeOfCustomers()

    println(CustomersData.typeOfCustomers())

    CustomersData.count = 109
    println(CustomersData.count)

    CustomersData.myMethod("hello")
}

open class MySuperClass {

    open fun myMethod(str: String) {
        println("MySuperClass")
    }
}

object CustomersData: MySuperClass() {      // Object Declaration

    var count: Int = -1             // Behaves like a STATIC variable

    init {
        //code
    }

    fun typeOfCustomers(): String { // Behaves like a STATIC method
        return "Indian"
    }

    override fun myMethod(str: String) {    // Currently, behaving like a STATIC method
        super.myMethod(str)
        println("object Customer Data: $str")
    }
}
