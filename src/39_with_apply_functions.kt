
/*
*   1. 'with' function: part of Kotlin liberary
*   2. 'apply' function
*
*   both keyword with and apply are part of Kotlin liberary
* */
fun main(args: Array<String>) {

    var person = Perrson()
//    person.name = "Steve"
//    person.age = 23

    with(person) { //to be concise, don't repeat person twice
        name = "Steve"
        age = 23
    } //cannot directly call .startRun() method

    person.apply { // start with person
        name = "Steve"
        age = 23
    }.startRun() // it start to call method directly

    println(person.name)
    println(person.age)
}

class Perrson {

    var name: String = ""
    var age: Int = -1

    fun startRun() {
        println("Now I am ready to run")
    }
}