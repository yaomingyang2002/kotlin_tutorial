

/*
* WHEN as Expression
* */
fun main(args: Array<String>) {

    val x = 100

    val str: String  = when (x) {

        0, 1 -> "x is 0 OR 1"
        in 20..100 -> "x is within 20 to 100"
        2 -> "x is 2"
        else -> {
            "x value is unknown"
            "x is an alien"
        }
    }

    println(str)
}
