
/*
    EXAMPLE TWO

*   1. Lambda Expression
*   2. Higher-Order Function
*
*   various ways to pass Lambda to high level functions
* */
fun main(args: Array<String>) {

    val program = MyProgram()

//    val myLambda: (Int, Int) -> Int = { x, y -> x + y}  // Lambda Expression [ Function ]
//    program.addTwoNumbers(2, 7, myLambda) // passing lambda to high level function
// OR,
//    program.addTwoNumbers(2, 7, { x, y -> x + y })
// OR,
    program.addTwoNumbers(2, 7) {x, y -> x + y}
}

class MyProgram {

    fun addTwoNumbers(a: Int, b: Int, action: (Int, Int) -> Int) {  // High Level Function with Lambda as Parameter
//    fun addTwoNumbers(a: Int, b: Int, myFunc: (Int, Int) -> Int) {  // High Level Function with Lambda as Parameter

        val result = action(a, b)
        println(result)
    }
}
