
/*
*   1. Method Overriding
*   2. Property Overriding
* */
fun main(args: Array<String>) {

    var dog = MyDog()

    println(dog.color)

    dog.eat()
}

open class MyAnimal { // open keyword

    open var color: String = "White" // open keyword

    open fun eat() {// open keyword
        println("Animal Eating")
    }
}

class MyDog : MyAnimal() { // inheritate

    var bread: String = ""  //extension

    override var color: String = "Black" // override used for overriding

    fun bark() { // extension
        println("Bark")
    }

    override fun eat() {
        super<MyAnimal>.eat() //|super.eat(), super<MyAnimal>.eat()  in case of multiple parents
        println("Dog is eating")
    }
}
