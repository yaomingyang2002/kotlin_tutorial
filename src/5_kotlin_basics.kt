

/*
* This is main function. Entry point of the application.
* */
fun main(args: Array<String>) {

    var personObj = Person() //don't need NEW keyword
    personObj.name = "Steve"

    println("The name of the person is "+ personObj.name)
    print("The name of the person is ${personObj.name}") //string interporation
}

class Person {

    var name: String = ""
}
