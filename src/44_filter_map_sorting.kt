/**
 * FILTER
 * Returns a list containing only elements matching the given [predicate]
 * "filter": filter our desired elements from a collection

 * MAP
 * Returns a list containing the results of applying the given [transform] function
 * to each element in the original collection
 * "map": perform operations, modify elements
 *
 * Predicates: Or conditions that returns TRUE or FALSE:
 *  -> "all": Do all elements satisfy the predicate/condition?
 *  -> "any": Do any element in list satisfy the predicate?
 *  ->"count": Total elements that satisfy the predicate
 *  ->"find": Returns the FIRT element that satisfy predicate
 *
 * More Functions
 * ->"flatmap": Fiter elements from a Collection of Collection
 * ->"distinct": Returns unique results
 * */

fun main(args: Array<String>) {

    val myNumbers: List<Int> = listOf(2, 3, 4, 6, 23, 90)

    val mySmallNums = myNumbers.filter { it < 10 }    // OR { num -> num < 10 }, it keyword lambda exp
    for (num in mySmallNums) {
        println(num)
        println()
    }

    val mySquaredNums = myNumbers.map { it * it }     // OR { num -> num * num }
    for (num in mySquaredNums) {
        println(num)
        println()
    }

    val mySmallSquaredNums =myNumbers.filter { it < 10 }
                                        .map { it * it }
    for (num in mySmallSquaredNums) {
        println(num)
        println()
    }

    var people = listOf<Pperson>(Pperson(10, "Steve"), Pperson(23, "Annie"), Pperson(17, "Sam"))
    var names = people.filter { person ->person.name.startsWith("S") } //{ it.name.startsWith("S") }
                        .map { it.name }

    for (name in names) {
        println(name)
    }
}

class Pperson(var age: Int, var name: String) {
    // Some other code..
}
